package Map;

import Config.Config;
import Config.HPConfig;
import Textures.MapTextures;

import java.awt.*;

public class Block {
    private final Image texture;
    private final int type;
    private final int hp;

    public Block(int type){
        this.type = type;
        texture = MapTextures.get("" + type);
        hp = HPConfig.map.getOrDefault(type, 0);
    }

    public int getType(){
        return type;
    }

    public boolean damage(int damage){
        double rand = Config.random.nextDouble();
        return rand < Math.pow(1. * damage/hp, 2);
    }

    public Image getTexture(){
        return texture;
    }
}
