package Map;

import Config.Config;
import Textures.MapTextures;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;

import static Config.Config.*;

public class Map {
    private final Block[][] blocks;
    private Image image;

    public Map(int[][] arr, Image image) {
        blocks = new Block[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++)
                blocks[i][j] = new Block(arr[i][j]);
        }
        if(image == null) {
            generateImage();
        }
        else this.image = image;
    }

    public Map(int width, int height) {
        blocks = new Block[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++)
                blocks[i][j] = new Block(0);
        }
        generateImage();
    }

    private void generateImage(){
        this.image = new BufferedImage((MAP_WIDTH + 2 * HORIZONTAL_BORDER) * BLOCK_SIZE, MAP_HEIGHT * BLOCK_SIZE, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = ((BufferedImage) this.image).createGraphics();
        for (int i = -HORIZONTAL_BORDER; i < MAP_WIDTH + HORIZONTAL_BORDER; i++)
            for (int j = 0; j < blocks[0].length; j++)
                g.drawImage(blocks[(i + MAP_WIDTH) % MAP_WIDTH][j].getTexture(), new AffineTransform(1, 0, 0, 1, (i + HORIZONTAL_BORDER) * BLOCK_SIZE, j * BLOCK_SIZE), null);
        g.dispose();
    }

    public void render(Graphics2D g){
        g.drawImage(image, -HORIZONTAL_BORDER*BLOCK_SIZE,0, null);
    }

    public Block getBlock(int i, int j){
        try{
            return blocks[i % MAP_WIDTH < 0 ? i % MAP_WIDTH + MAP_WIDTH : i % MAP_WIDTH][j];
        } catch (Exception e){return null;}
    }

    public void setBlock(Block b, int i, int j){
        int n = (int)Math.floor(1. * i / MAP_WIDTH);
        i -= n * MAP_WIDTH;
        if(j >= 0 && j < MAP_HEIGHT) {
            blocks[i][j] = b;
            Graphics2D g = ((BufferedImage) image).createGraphics();
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC));
            g.drawImage(b.getTexture(), (i + HORIZONTAL_BORDER) * BLOCK_SIZE, j * BLOCK_SIZE, null);
            if (i >= MAP_WIDTH - HORIZONTAL_BORDER)
                g.drawImage(b.getTexture(), (i - MAP_WIDTH + HORIZONTAL_BORDER) * BLOCK_SIZE, j * BLOCK_SIZE, null);
            if (i < HORIZONTAL_BORDER)
                g.drawImage(b.getTexture(), (i + MAP_WIDTH + HORIZONTAL_BORDER) * BLOCK_SIZE, j * BLOCK_SIZE, null);
            g.dispose();
            //updateRainMap(i, j);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void writeToFile(String path){
        try {
            int[][] types = new int[MAP_WIDTH][MAP_HEIGHT];
            for(int i = 0; i < MAP_WIDTH; i++)
                for(int j = 0; j < MAP_HEIGHT; j++)
                    types[i][j] = blocks[i][j].getType();
            File file1 = new File(path + File.separator + "newMap.obj");
            if (file1.exists()) file1.delete();
            file1.createNewFile();
            File file2 = new File(path + File.separator + "newMap.png");
            if (file2.exists()) file2.delete();
            file2.createNewFile();
            FileOutputStream fs = new FileOutputStream(file1);
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(types);
            os.close();
            fs.close();
            ImageIO.write(((BufferedImage)image).getSubimage(HORIZONTAL_BORDER * BLOCK_SIZE, 0, MAP_WIDTH * BLOCK_SIZE, MAP_HEIGHT * BLOCK_SIZE), "png", file2);
        } catch (Exception e){throw new RuntimeException(e);}
    }

    public static Map readFromFile(String path, String name){
        try {
            File file1 = new File(path + File.separator + name +".obj");
            File file2 = new File(path + File.separator + name + ".png");
            FileInputStream fs = new FileInputStream(file1);
            ObjectInputStream os = new ObjectInputStream(fs);
            int[][] types = (int[][])os.readObject();
            os.close();
            fs.close();
            Image image = ImageIO.read(file2);
            BufferedImage bufImage = new BufferedImage((MAP_WIDTH + 2 * HORIZONTAL_BORDER) * BLOCK_SIZE, MAP_HEIGHT * BLOCK_SIZE, BufferedImage.TYPE_INT_ARGB);
            Graphics g = bufImage.createGraphics();

            g.drawImage(image, (HORIZONTAL_BORDER - MAP_WIDTH) * BLOCK_SIZE, 0, null);
            g.drawImage(image, HORIZONTAL_BORDER * BLOCK_SIZE, 0, null);
            g.drawImage(image, (HORIZONTAL_BORDER + MAP_WIDTH) * BLOCK_SIZE, 0, null);
            return new Map(types, bufImage);
        } catch (Exception e){throw new RuntimeException(e);}
    }
}
