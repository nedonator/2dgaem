package Map;

import Player.Player;

import java.awt.*;

import static Config.Config.HEIGHT;
import static Config.Config.WIDTH;

public class Layer {
    private Image image;
    private double xCoef;
    private double yCoef;
    private double x;
    private double y;
    private Player player;
    private int width;

    public Layer(Image image, double xCoef, double yCoef, Player player, int width) {
        this.image = image;
        this.xCoef = xCoef;
        this.yCoef = yCoef;
        this.player = player;
        x = -WIDTH/2;
        y = -HEIGHT/2;
        this.width = width;
    }

    public void update(double dt){
        x -= xCoef * player.getVx() * dt;
        y = -HEIGHT/2. -yCoef * player.getY();
        int n = (int)Math.floor(x / width);
        x -= n * width;
    }

    public void render(Graphics g){
        g.drawImage(image, (int)x - width / 2, (int)y, null);
        g.drawImage(image, (int)x - 3 * width / 2, (int)y, null);
    }
}
