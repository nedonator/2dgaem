package Player;

import App.App;
import Weapons.SemiAutomaticWeaponConfig;
import Weapons.Weapon;
import sources.BattleScene;
import sources.WeaponConfig;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PlayerConfig implements Serializable {
    private static PlayerConfig playerConfig = init();
    private int maxHp;
    private double maxMana;
    private double totalExp;
    private List<WeaponConfig> weapons;
    private int money;

    private static PlayerConfig init(){
        File file = new File("save");
        try {
            ObjectInputStream os = new ObjectInputStream(new FileInputStream(file));
            Object o = os.readObject();
            os.close();
            return (PlayerConfig)o;
        } catch(Exception e){
            return new PlayerConfig();
        }
    }

    private PlayerConfig(){
        maxHp = 100;
        maxMana = 0;
        totalExp = 0;
        weapons = new ArrayList<>();
        weapons.add(new SemiAutomaticWeaponConfig(25, 0, 150, new Color(255, 100, 50)));
        money = 0;
    }

    public static Player getPlayer(App app, BattleScene scene, JFrame frame, int spawnX, int spawnY){
        Weapon[] playerWeapons = new Weapon[playerConfig.weapons.size()];
        for(int i = 0; i != playerConfig.weapons.size(); i++){
            playerWeapons[i] = playerConfig.weapons.get(i).getWeapon(scene, frame);
        }
        return new Player(app, scene, playerConfig.maxHp, (int)playerConfig.maxMana, playerWeapons, spawnX, spawnY);
    }

    public static void save(){
        File file = new File("save");
        try{
            if(!file.exists())
                file.createNewFile();
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file));
            os.writeObject(playerConfig);
        } catch(Exception e){e.printStackTrace();}
    }

    public static void addExp(int exp){
        double newTotalExp = Math.sqrt(Math.pow( playerConfig.totalExp, 2) + exp);
        playerConfig.maxMana += newTotalExp -  playerConfig.totalExp;
        playerConfig.totalExp = newTotalExp;
    }

    public static void addMoney(int money){
        playerConfig.money += money;
    }

    public static boolean takeMoney(int money){
        if( playerConfig.money >= money){
            playerConfig.money -= money;
            return true;
        } else return false;
    }

    public static void addWeapon(WeaponConfig weapon){
        playerConfig.weapons.add(weapon);
    }

    public static void addHp(){
        if( playerConfig.maxMana >= 10){
            playerConfig.maxMana -= 10;
            playerConfig.maxHp += 10;
        }
    }

    public static void removeHp(){
        if( playerConfig.maxHp >= 110){
            playerConfig.maxMana += 10;
            playerConfig.maxHp -= 10;
        }
    }

    public static int getMaxHp() {
        return playerConfig.maxHp;
    }

    public static int getMaxMana() {
        return (int)playerConfig.maxMana;
    }

    public static int getMoney() {
        return playerConfig.money;
    }

    public static void refresh(){
        playerConfig = new PlayerConfig();
    }
}
