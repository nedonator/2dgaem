package Player;

import App.App;
import Config.Config;
import Objects.ConstantSprite;
import Weapons.Bullet;
import Objects.Damageable;
import Objects.Landable;
import Textures.MapTextures;
import Weapons.SwitchableMouseListener;
import Weapons.Weapon;
import sources.*;

import java.awt.*;
import java.awt.event.MouseListener;
import java.io.File;

import static Config.Config.*;


public class Player extends Landable implements Damageable {
    private final PlayerKeyListener keyListener;
    private final SwitchableMouseListener mouseListener;
    private final Weapon[] weapons;
    private int weaponNum = 0;
    private double maxMana;
    private double mana;
    private double maxHp;
    private double hp;
    private int minTemp = 273;
    private int maxTemp = 330;
    private double speedMult = 1;

    public Player(App app, BattleScene scene, int maxHp, int maxMana, Weapon[] weapons, double x, double y){
        this.x = x;
        this.y = y;
        hp = this.maxHp = maxHp;
        mana = this.maxMana = maxMana;
        this.weapons = weapons;
        vx = 0;
        vy = 1;
        texture = MapTextures.get("player");
        this.scene = scene;
        width = 2;
        height = 3;
        keyListener = new PlayerKeyListener(app, scene);

        traceColor = new Color(255, 0, 0);
        xBias = width/2;
        yBias = 1;
        mouseListener = new SwitchableMouseListener(this.weapons[0].getMouseListener());
    }

    @Override
    public void update(double dt){
        super.update(dt);
        if(mana < maxMana)mana += dt*MANA_REGEN;
        if(mana > maxMana)mana = maxMana;
        if(Math.abs(vx) < dt* Config.a/2)vx = 0;
        double ax = isLanded ? -Math.signum(vx)*Config.a/2 : 0;
        double ay = 0;
        if(keyListener.isAPressed()) ax -= Config.a * speedMult;
        if(keyListener.isDpressed()) ax += Config.a * speedMult;
        if(keyListener.isWPressed() && isLanded){
            vy -= JUMP_SPEED * speedMult;
            isLanded = false;
        }
        if(keyListener.isSpressed()) ay += Config.g;
        vx += ax*dt;
        vy += ay*dt;
        weapons[weaponNum].update(dt);
    }

    public PlayerKeyListener getKeyListener() {
        return keyListener;
    }

    public MouseListener getMouseListener() {
        return mouseListener;
    }

    public void setWeaponNum(int n){
        System.out.println("weapon " + n + " enabled");
        if(n > 0 && n <= weapons.length){
            weaponNum = n-1;
            mouseListener.setMouseListener(weapons[weaponNum].getMouseListener());
        }
    }

    public boolean consume(double mana){
        if(this.mana >= mana){
            this.mana -= mana;
            return true;
        }
        else return false;
    }

    public double getHp() {
        return hp;
    }

    public double getMaxHp() {
        return maxHp;
    }

    @Override
    public void tryDamage(int damage){
        hp -= damage;
        RenderDamage.render(scene, damage);
    }

    @Override
    public void damage(int damage) {
        hp -= damage;
    }

    public void damage(double d) {
        damage((int)Math.floor(d));
        if(random.nextDouble() < d % 1)damage(1);
    }

    @Override
    public void tryHit(Bullet b){
        if(b.getY() <= y + height && b.getY() >= y && !b.isPlayers()){
            this.damage(b.getDamage());
            scene.delete(b);
        }
    }

    public double getMaxMana() {
        return maxMana;
    }

    public double getMana() {
        return mana;
    }

    public int getAmmo(){
        return weapons[weaponNum].getAmmo();
    }

    @Override
    public void tempAffect(double temp, double dt) {
        if(temp < minTemp){
            speedMult = temp / minTemp;
            damage((minTemp - temp)/minTemp * 100 * dt);
            scene.setTempEffect(new ConstantSprite(scene, -WIDTH/2./BLOCK_SIZE,
                    -HEIGHT/2./BLOCK_SIZE, 0, 0,
                    MapTextures.get("DamageEffects" + File.separator + "frost"),
                    Math.min((minTemp - temp)/minTemp + 0.2, 1)));
        }else{
            speedMult = 1;
            scene.setTempEffect(null);
        }

        if(temp > maxTemp) {
            damage((temp - maxTemp) / maxTemp * 100 * dt);
            scene.setTempEffect(new ConstantSprite(scene, -WIDTH / 2. / BLOCK_SIZE,
                    -HEIGHT / 2. / BLOCK_SIZE, 0, 0,
                    MapTextures.get("DamageEffects" + File.separator + "heat"),
                    Math.min(1, (temp - maxTemp) / maxTemp + 0.2)));
        }
    }
}
