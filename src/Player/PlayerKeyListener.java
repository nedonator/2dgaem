package Player;

import App.App;
import sources.BattleScene;
import sources.WASDKeyListener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.VK_ESCAPE;

public class PlayerKeyListener extends WASDKeyListener implements KeyListener {
    private final App app;
    private final BattleScene scene;

    public PlayerKeyListener(App app, BattleScene scene){
        this.app = app;
        this.scene = scene;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        if(e.getKeyCode() == VK_ESCAPE){
            app.terminate();
        }
        else if(Character.isDigit(e.getKeyChar())){
            scene.getPlayer().setWeaponNum(Character.digit(e.getKeyChar(), 10));
        }
    }
}