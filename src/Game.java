import App.App;
import Map.Map;
import MapEditor.MapEditorScene;
import sources.BattleScene;
import sources.MainMenu;
import sources.Scene;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import static Config.Config.HEIGHT;
import static Config.Config.WIDTH;


public class Game implements Runnable, App {

    private final JFrame frame;
    private final BufferStrategy bufferStrategy;
    private final Canvas canvas;
    private MainMenu mainMenu;

    public Game(){
        frame = new JFrame("Basic Game");
        frame.setUndecorated(true);
        JPanel panel = (JPanel) frame.getContentPane();
        panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        panel.setLayout(null);

        canvas = new Canvas();
        canvas.setBounds(0, 0, WIDTH, HEIGHT);
        canvas.setIgnoreRepaint(true);

        panel.add(canvas);

        mainMenu = new MainMenu(this, frame);
        scene = mainMenu;
        //scene = new BattleScene(this, frame, Map.readFromFile("maps"), 15, 0);
        //canvas.addMouseListener(new MouseControl());
        //scene = new MapEditorScene(this, frame);
        canvas.addKeyListener(scene.getKeyListener());
        canvas.addMouseListener(scene.getMouseListener());

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);

        canvas.createBufferStrategy(2);
        bufferStrategy = canvas.getBufferStrategy();

        canvas.requestFocus();

    }


    private class MouseControl extends MouseAdapter{

    }

    private boolean running = true;

    @Override
    public boolean isRunning() {
        return running;
    }

    private Scene scene;

    public void run(){

        long beginLoopTime;
        long endLoopTime;
        long currentUpdateTime = System.nanoTime();
        long lastUpdateTime;
        long deltaLoop;

        while(running){
            beginLoopTime = System.nanoTime();

            render();

            lastUpdateTime = currentUpdateTime;
            currentUpdateTime = System.nanoTime();
            update((int) ((currentUpdateTime - lastUpdateTime)/(1000*1000)));

            endLoopTime = System.nanoTime();
            deltaLoop = endLoopTime - beginLoopTime;

            long desiredFPS = 1000;
            long desiredDeltaLoop = (1000 * 1000 * 1000) / desiredFPS;
            if(deltaLoop < desiredDeltaLoop){
                try{
                    Thread.sleep((desiredDeltaLoop - deltaLoop)/(1000*1000));
                }catch(InterruptedException e){
                    //Do nothing
                }
            }
        }
    }

    @Override
    public void render() {
        Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();
        g.clearRect(0, 0, WIDTH, HEIGHT);
        g.translate(WIDTH/2, HEIGHT/2);
        scene.render(g);
        g.dispose();
        bufferStrategy.show();
    }

    @Override
    public void terminate() {
        scene.save();
        if(scene == mainMenu) {
            running = false;
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        } else setScene(mainMenu);
    }

    /**
     * Rewrite this method for your game
     */
    @Override
    public void update(int deltaTime){
        scene.update(deltaTime/1000.);
    }

    @Override
    public void setScene(Scene scene) {
        canvas.removeMouseListener(this.scene.getMouseListener());
        canvas.removeKeyListener(this.scene.getKeyListener());
        this.scene = scene;
        canvas.addKeyListener(scene.getKeyListener());
        canvas.addMouseListener(scene.getMouseListener());
    }

    @Override
    public JFrame getFrame() {
        return frame;
    }
}