package Objects;

import Weapons.Bullet;

public interface Damageable {
    void tryDamage(int damage);
    void damage(int damage);
    void tryHit(Bullet bullet);
    void tempAffect(double temp, double dt);
    void damage(double damage);
}
