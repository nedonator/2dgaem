package Objects;

import Config.Config;
import sources.BattleScene;

import java.util.Arrays;
import java.util.List;

public class EnemyGenerator {
    private static List<EnemyConfig> list = Arrays.asList(
            new EnemyConfig("little_ember", 0.5, 0.5, 50, 10, 10, 0, 100),
            new EnemyConfig("enemy", 2, 3, 150, 30, 50, Config.g, 300)
    );

    public static Enemy generate(BattleScene scene, double x, double y, double temp){
        double sum = 0;
        for(EnemyConfig e : list){
            double w = w(temp, e);
            sum += w;
        }
        for(EnemyConfig e : list){
            if(Config.random.nextDouble() < w(temp, e)/sum)
                return e.create(scene, x, y);
            else sum -= w(temp, e);
        }
        return list.get(0).create(scene, x, y);
    }

    private static double w(double temp, EnemyConfig e){
        return Math.exp(-4*Math.abs(temp - e.getTemp())/e.getTemp());
    }
}
