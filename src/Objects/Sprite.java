package Objects;

import Config.Config;
import sources.BattleScene;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class Sprite extends Movable {
    private double lifetime;
    private final double initLifetime;
    public Sprite(BattleScene scene, double x, double y, float vx, float vy, Image texture, double lifetime){
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.texture = texture;
        this.lifetime = lifetime;
        initLifetime = lifetime;
    }
    public boolean isAlive(){
        return lifetime > 0;
    }
    @Override
    public void update(double dt){
        super.update(dt);
        lifetime -= dt;
    }
    @Override
    public void render(Graphics2D g){
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,(float)(lifetime/initLifetime)));
        g.drawImage(texture, new AffineTransform(1,0,0,1,x* Config.BLOCK_SIZE,y*Config.BLOCK_SIZE),null);
    }
}
