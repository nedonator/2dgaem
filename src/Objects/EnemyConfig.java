package Objects;

import Textures.MapTextures;
import sources.BattleScene;

import java.awt.*;

public class EnemyConfig {
    private String texture;
    private double width;
    private double height;
    private int hp;
    private int damage;
    private int exp;
    private double g;
    private double temp;

    public EnemyConfig(String texture, double width, double height, int hp, int damage, int exp, double g, double temp) {
        this.texture = texture;
        this.width = width;
        this.height = height;
        this.hp = hp;
        this.damage = damage;
        this.exp = exp;
        this.g = g;
        this.temp = temp;
    }

    public Enemy create(BattleScene scene, double x, double y){
        return new Enemy(scene, x, y, MapTextures.get(texture), width, height, hp, damage, exp, g, temp);
    }

    public double getTemp(){
        return temp;
    }
}
