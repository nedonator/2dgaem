package Objects;

import Config.Config;
import sources.BattleScene;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class ConstantSprite extends Sprite {
    private double alpha;
    public ConstantSprite(BattleScene scene, double x, double y, float vx, float vy, Image texture, double alpha){
        super(scene, x, y, vx, vy, texture, 0);
        this.alpha = alpha;
    }

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public void render(Graphics2D g) {
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,(float)alpha));
        g.drawImage(texture, new AffineTransform(1,0,0,1,x* Config.BLOCK_SIZE,y*Config.BLOCK_SIZE),null);
    }
}
