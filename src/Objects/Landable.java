package Objects;

import Config.Config;

import java.awt.*;
import java.awt.geom.AffineTransform;

import static java.lang.StrictMath.round;
import static java.lang.StrictMath.floor;
import static Config.Config.BLOCK_SIZE;
import static Config.Config.TRACE_LEN;

public class Landable extends Movable {
    protected boolean isLanded = false;
    protected double width;
    protected double height;
    protected int direction = 1;
    protected double g = Config.g;

    private boolean check(int from, int to, int other, int objectSide, boolean axis){
        for(; from < to; from++){
            if(axis){
                if(scene.getType(from, other) != 0 && scene.getType(from, other + objectSide) == 0){
                    //y = round(y);
                    vy = 0;
                    return true;
                }
            } else{
                if(scene.getType(other, from) != 0 && scene.getType(other + objectSide, from) == 0){
                    //x = round(x);
                    vx = 0;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void update(double dt) {
        if (!isLanded) {
            vy += g * dt;
        }
        isLanded = false;
        if (vy >= 0) {
            int i = (int) round(floor(x));
            int j = (int) round(floor(x + width + 1));
            int h = (int) round(floor(y + height));
            if(check(i, j, h, -1, true)){
                isLanded = true;
                y = round(y + height) - height;
            }
            for (; i < j; i++) {
                int bot = (int) Math.round(y + height) - 1;
                if (scene.getType(i, bot) != 0 && scene.getType(i, bot - 1) == 0 &&
                        !check((int) round(floor(x)), (int) round(floor(x + width + 1)), (int) round(floor(y) - 1), 1, true)) y -= 1;
            }
        }
        if (vx < 0)
            if(check((int) round(y), (int) round(y + height), (int) round(floor(x)), 1, false))
                x = round(x);
        if (vx > 0)
            if(check((int) round(y), (int) round(y + height), (int) round(floor(x + width)), -1, false))
                x = round(x + width) - width;
        if (vy < 0)
            if(check((int) round(floor(x)), (int) round(floor(x + width + 1)), (int) round(floor(y)), 1, true))
                y = round(y);
        super.update(dt);
        if(vx != 0)
            direction = (int)Math.signum(vx);
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public void render(Graphics2D g) {
        renderTrace(g);
        g.drawImage(texture, new AffineTransform(direction,0,0,1,(x - width*(direction - 1)/2)*Config.BLOCK_SIZE,y*Config.BLOCK_SIZE),null);
    }
}
