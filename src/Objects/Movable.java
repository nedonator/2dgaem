package Objects;

import Config.Config;
import sources.BattleScene;

import java.awt.*;
import java.awt.geom.AffineTransform;

import static Config.Config.*;

public class Movable {
    protected double x;
    protected double y;
    protected BattleScene scene;
    protected double vx;
    protected double vy;
    protected double xBias;
    protected double yBias;
    protected Image texture;
    protected Color traceColor;

    public void update(double dt){
        x += vx * dt;
        y += vy * dt;
    }

    public void normalize(){
        int n = (int)(this == scene.getPlayer() ? Math.floor(x/MAP_WIDTH) : Math.floor((x + MAP_WIDTH/2 - scene.getPlayer().getX())/MAP_WIDTH));
        if(n != 0)
            x -= n * MAP_WIDTH;
    }

    public void renderTrace(Graphics2D g){
        g.setColor(traceColor);
        g.setStroke(new BasicStroke(3));
        g.drawLine((int)((x + xBias - vx * TRACE_LEN) * BLOCK_SIZE), (int)((y + yBias - vy * TRACE_LEN) * BLOCK_SIZE), (int)((x + xBias) * BLOCK_SIZE), (int)((y + yBias) * BLOCK_SIZE));
    }

    public void render(Graphics2D g){
        renderTrace(g);
        g.drawImage(texture, new AffineTransform(1,0,0,1,x* Config.BLOCK_SIZE,y*Config.BLOCK_SIZE),null);
    }

    public double getxBias() {
        return xBias;
    }

    public double getyBias() {
        return yBias;
    }

    public double getVx() {
        return vx;
    }

    public double getVy() {
        return vy;
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }
}
