package Objects;

import Config.Config;
import Weapons.Bullet;
import sources.BattleScene;

import java.awt.*;

import static Config.Config.ENEMY_SPEED;
import static Config.Config.random;

public class Enemy extends Landable implements Damageable {
    private int hp;
    private double updateTime = 0;
    private int direction = 0;
    private int futureDirection = 0;
    private int damage;
    private int exp;
    private double temp;

    public Enemy(BattleScene scene, double x, double y, Image texture, double width, double height, int hp, int damage, int exp, double temp){
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.vx = 0;
        this.vy = 0;
        this.texture = texture;
        this.width = width;
        this.height = height;
        this.hp = hp;
        xBias = width/2;
        yBias = 1;
        this.damage = damage;
        this.exp = exp;
        this.traceColor = new Color(0, 0, 0, 0);
        this.temp = temp;
    }

    public Enemy(BattleScene scene, double x, double y, Image texture, double width, double height, int hp, int damage, int exp, double g, double temp){
        this(scene, x, y, texture, width, height, hp, damage, exp, temp);
        this.g = g;
    }

    @Override
    public void update(double dt) {
        super.update(dt);
        vx = ENEMY_SPEED * direction;
        if(Math.signum(scene.getPlayer().x - x) != futureDirection){
            futureDirection = (int)Math.signum(scene.getPlayer().x - x);
            updateTime = random.nextDouble();
            if(Math.abs(scene.getPlayer().y - y) < height && Math.abs(scene.getPlayer().x - x) < Config.MAP_WIDTH/2.)
                scene.getPlayer().tryDamage(damage);
        }
        if(updateTime <= 0){
            direction = futureDirection;
            vy = Math.signum(scene.getPlayer().y - y) * 2;
            updateTime = random.nextDouble();
        }
        updateTime -= dt;
    }

    @Override
    public void normalize() {
        super.normalize();
        if(Math.signum(scene.getPlayer().x - x) != futureDirection) {
            direction = futureDirection = (int) Math.signum(scene.getPlayer().x - x);
        }
    }

    @Override
    public void tryHit(Bullet b){
        if(b.y <= y + height && b.y >= y && b.isPlayers()){
            this.tryDamage(b.getDamage());
            scene.delete(b);
        }
    }

    @Override
    public void damage(double d) {
        damage((int)Math.floor(d));
        if(random.nextDouble() < d % 1)damage(1);
    }

    @Override
    public void damage(int damage){
        hp -= damage;
        if(hp <= 0)
            scene.delete(this);
    }

    public int getExp() {
        return exp;
    }

    @Override
    public void tempAffect(double temp, double dt) {
        double d = temp / this.temp;
        if(d < 0.5)damage((10 - d*20)*dt);
        if(d > 3)damage((2*d - 6)*dt);
    }

    @Override
    public void tryDamage(int damage) {
        damage(damage);
    }
}
