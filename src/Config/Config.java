package Config;

import java.util.Random;

public class Config {
    public static final Random random = new Random();
    public static final int WIDTH = 800;
    public static final int HEIGHT = 500;
    public static final int BLOCK_SIZE = 16;
    public static final double a = 25;
    public static final double g = 40;
    public static final double JUMP_SPEED = 30;
    public static final int MAP_WIDTH = 50;
    public static final int MAP_HEIGHT = 30;
    public static final int HORIZONTAL_BORDER = 25;
    public static final double MAP_EDITOR_SPEED = 250;
    public static final double BULLET_SPEED = 120;
    public static final double TRACE_LEN = 0.01;
    public static final double TEMP_FACTOR = 0.1f;
    public static final double COOL_TIME = 1.2;
    public static final double MANA_REGEN = 15;
    public static final double ENEMY_SPEED = 8;
}
