package Weapons;

import sources.BattleScene;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SemiAutomaticMouseListener implements MouseListener {
    private final BattleScene scene;
    private final int damage;
    private final double manacost;
    private final SemiAutomaticWeapon weapon;
    private final Color color;

    public SemiAutomaticMouseListener(BattleScene scene, SemiAutomaticWeapon weapon, int damage, double manacost, Color color) {
        this.scene = scene;
        this.damage = damage;
        this.manacost = manacost;
        this.weapon = weapon;
        this.color = color;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(scene.getPlayer().consume(manacost) && weapon.consumeAmmo()){
            Bullet.shoot(scene, e.getPoint().x, e.getPoint().y, damage, 0, true, color);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
