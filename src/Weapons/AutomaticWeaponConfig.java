package Weapons;

import Config.Config;
import sources.BattleScene;
import sources.WeaponConfig;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class AutomaticWeaponConfig implements Serializable, WeaponConfig {
    private int damage;
    private int fireRate;
    private int manacost;
    private int ammo;
    private Color color;

    public AutomaticWeaponConfig() {
        damage = 5;
        fireRate = 10;
        if (Config.random.nextBoolean()) {
            manacost = 1;
            ammo = -1;
        } else {
            manacost = 0;
            ammo = 100;
        }
        color = new Color(Config.random.nextInt() % 255, Config.random.nextInt() % 255, Config.random.nextInt() % 255);
    }

    public AutomaticWeaponConfig(int damage, int fireRate, int manacost, int ammo, Color color) {
        this.damage = damage;
        this.fireRate = fireRate;
        this.manacost = manacost;
        this.ammo = ammo;
        this.color = color;
    }

    @Override
    public Weapon getWeapon(BattleScene scene, JFrame frame) {
        return new AutomaticWeapon(scene, frame, damage, fireRate, manacost, ammo, color);
    }
}
