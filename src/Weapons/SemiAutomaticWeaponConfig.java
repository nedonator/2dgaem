package Weapons;

import Config.Config;
import sources.BattleScene;
import sources.WeaponConfig;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class SemiAutomaticWeaponConfig implements Serializable, WeaponConfig {
    private int damage;
    private int manacost;
    private int ammo;
    private Color color;

    public SemiAutomaticWeaponConfig() {
        damage = 25;
        if (Config.random.nextBoolean()) {
            manacost = 0;
            ammo = 20;
        } else {
            manacost = 10;
            ammo = -1;
        }
        color = new Color(Config.random.nextInt() % 255, Config.random.nextInt() % 255, Config.random.nextInt() % 255);
    }

    public SemiAutomaticWeaponConfig(int damage, int manacost, int ammo, Color color) {
        this.damage = damage;
        this.manacost = manacost;
        this.ammo = ammo;
        this.color = color;
    }

    @Override
    public Weapon getWeapon(BattleScene scene, JFrame frame) {
        return new SemiAutomaticWeapon(scene, damage, manacost, ammo, color);
    }
}
