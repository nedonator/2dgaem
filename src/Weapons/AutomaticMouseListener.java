package Weapons;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class AutomaticMouseListener implements MouseListener {
    private boolean isPressed = false;
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        isPressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        isPressed = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {
        isPressed = false;
    }

    public boolean isPressed(){
        return isPressed;
    }
}
