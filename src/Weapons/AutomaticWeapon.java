package Weapons;

import sources.BattleScene;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

import static Config.Config.*;

public class AutomaticWeapon extends Weapon {
    private float temperature = 0;
    private int damage;
    private int fireRate;
    private AutomaticMouseListener automaticMouseListener = new AutomaticMouseListener();
    private double bullets = 0;
    private Color color;
    private JFrame frame;

    public AutomaticWeapon(BattleScene scene, JFrame frame, int damage, int fireRate, double manacost, int ammo, Color color) {
        this.scene = scene;
        this.frame = frame;
        this.damage = damage;
        this.fireRate = fireRate;
        this.manacost = manacost;
        this.ammo = ammo;
        this.color = color;
    }

    @Override
    public MouseListener getMouseListener() {
        return automaticMouseListener;
    }

    @Override
    public void update(double dt) {

        if(automaticMouseListener.isPressed()) {
            bullets += dt * fireRate;
            int n = (int)bullets;
            bullets -= n;
            while (n-- != 0) {
                if (!scene.getPlayer().consume(manacost) || ammo == 0) break;
                Bullet.shoot(scene, frame.getMousePosition().x,
                        frame.getMousePosition().y, damage, temperature * TEMP_FACTOR, true, color);
                temperature += 1;
                if(ammo > 0)ammo--;
            }
        }

            temperature *= 1 - dt/COOL_TIME;
    }


}
