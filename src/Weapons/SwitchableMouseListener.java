package Weapons;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SwitchableMouseListener implements MouseListener {
    private MouseListener mouseListener;

    public SwitchableMouseListener(MouseListener mouseListener){
        this.mouseListener = mouseListener;
    }

    public void setMouseListener(MouseListener mouseListener){
        this.mouseListener = mouseListener;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouseListener.mouseClicked(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mouseListener.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mouseListener.mouseReleased(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseListener.mouseEntered(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseListener.mouseExited(e);
    }
}
