package Weapons;

import Objects.Movable;
import Player.Player;
import Map.Block;
import Config.Config;
import sources.BattleScene;

import java.awt.*;

import static Config.Config.*;

public class Bullet extends Movable {
    private int damage;
    private boolean isPlayers;

    public Bullet(BattleScene scene, int damage, double x, double y, double vx, double vy, boolean isPlayers, Color color) {
        this.scene = scene;
        this.damage = damage;
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        traceColor = color;
        xBias = 1./8;
        yBias = 1./8;
        this.isPlayers = isPlayers;
    }

    @Override
    public void update(double dt) {
        super.update(dt);
        Block block = scene.getMap().getBlock((int)x, (int)y);
        if(block != null && block.getType() != 0){
            if(block.damage(damage)) scene.setBlock(new Block(0), (int)x, (int)y);
            scene.delete(this);
        }
        vy += Config.g * dt;
        if(y > MAP_HEIGHT + 100)
            scene.delete(this);
    }

    public static void shoot(BattleScene scene, int x, int y, int damage, double dv, boolean isPlayers, Color color) {
        Player player = scene.getPlayer();
        double dx = x - WIDTH / 2.f - player.getxBias() * BLOCK_SIZE;
        double dy = y - HEIGHT / 2.f - BLOCK_SIZE;
        double n = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        if (n != 0)
            scene.create(new Bullet(scene, damage, player.getX() + player.getxBias(), player.getY() + player.getyBias(), BULLET_SPEED * dx / n + dv*(random.nextDouble()-0.5), BULLET_SPEED * dy / n + dv*(random.nextDouble()-0.5), isPlayers, color));
    }

    public int getDamage() {
        return damage;
    }

    public boolean isPlayers() {
        return isPlayers;
    }
}
