package Weapons;

import sources.BattleScene;

import java.awt.event.MouseListener;

public abstract class Weapon {
    protected BattleScene scene;
    protected double manacost;
    protected int ammo;

    public abstract MouseListener getMouseListener();

    public abstract void update(double dt);

    public double getManacost() {
        return manacost;
    }

    public int getAmmo() {
        return ammo;
    }
}
