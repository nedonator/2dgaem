package Weapons;

import sources.BattleScene;

import java.awt.*;
import java.awt.event.MouseListener;

public class SemiAutomaticWeapon extends Weapon {
    private BattleScene scene;
    private int damage;
    private Color color;

    public SemiAutomaticWeapon(BattleScene scene, int damage, double manacost, int ammo, Color color) {
        this.scene = scene;
        this.damage = damage;
        this.manacost = manacost;
        this.ammo = ammo;
        this.color = color;
    }

    @Override
    public MouseListener getMouseListener() {
        return new SemiAutomaticMouseListener(scene, this, damage, manacost, color);
    }

    @Override
    public void update(double dt) {

    }

    public boolean consumeAmmo(){
        boolean result = ammo != 0;
        if(ammo > 0)ammo--;
        return result;
    }
}
