package sources;

import App.App;
import Player.PlayerConfig;
import Textures.MapTextures;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import static Config.Config.HEIGHT;
import static Config.Config.WIDTH;

public class CharacterMenu implements Scene {
    private Image image;
    private JFrame frame;
    private ButtonMouseListener mouseListener;
    private MenuKeyListener keyListener;
    private Button[] buttons;

    public CharacterMenu(App app) {
        this.frame = app.getFrame();
        this.image = MapTextures.get("menu");
        buttons = new Button[2];
        buttons[0] = new Button(250, 170, 40, 40, MapTextures.get("minus"), PlayerConfig::removeHp);
        buttons[1] = new Button(450, 170, 40, 40, MapTextures.get("plus"), PlayerConfig::addHp);
        mouseListener = new ButtonMouseListener(buttons, frame);
        keyListener = new MenuKeyListener(app);
    }

    @Override
    public void update(double dt) {

    }

    @Override
    public void render(Graphics2D g) {
        g.translate(-WIDTH/2, -HEIGHT/2);
        g.drawImage(image, 0, 0, null);
        for(Button b : buttons)
            b.render(g);
        g.setColor(new Color(0, 220, 220));
        g.setFont(new Font("Dialog", Font.PLAIN, 36));
        g.drawString("mana: " + PlayerConfig.getMaxMana(), 300, 40);
        g.drawString("hp: " + PlayerConfig.getMaxHp(), 300, 200);
    }

    @Override
    public MouseListener getMouseListener() {
        return mouseListener;
    }

    @Override
    public KeyListener getKeyListener() {
        return keyListener;
    }


    @Override
    public void save() {
        PlayerConfig.save();
    }
}
