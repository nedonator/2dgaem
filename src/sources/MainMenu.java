package sources;

import App.App;
import MapEditor.MapEditorScene;
import Textures.MapTextures;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import static Config.Config.HEIGHT;
import static Config.Config.WIDTH;

public class MainMenu implements Scene, ButtonMenu {
    private JFrame frame;
    private Image image;
    private ButtonMouseListener mouseListener;
    private ButtonKeyListener keyListener;
    private Button[] buttons;

    public MainMenu(App app, JFrame frame){
        this.frame = frame;
        image = MapTextures.get("main_menu");
        buttons = new Button[3];
        buttons[0] = new Button(WIDTH/2-150, 200, 300, 60, MapTextures.get("to_battle"), ()->app.setScene(new MapSwitch(app)));
        buttons[1] = new Button(WIDTH/2-150, 300, 300, 60, MapTextures.get("character_menu"), ()->app.setScene(new CharacterMenu(app)));
        buttons[2] = new Button(WIDTH/2-150, 400, 300, 60, MapTextures.get("map_editor"), ()->app.setScene( new MapEditorScene(app)));
        mouseListener = new ButtonMouseListener(buttons, frame);
        keyListener = new ButtonKeyListener(app, this);
    }

    @Override
    public void update(double dt) {

    }

    @Override
    public void render(Graphics2D g) {
        g.translate(-WIDTH/2, -HEIGHT/2);
        g.drawImage(image, 0, 0, null);
        for(Button b : buttons)
            b.render(g);
    }

    @Override
    public MouseListener getMouseListener() {
        return mouseListener;
    }

    @Override
    public KeyListener getKeyListener() {
        return keyListener;
    }

    @Override
    public void save() {

    }

    @Override
    public void pressKey(int key) {
        if(key > 0 && key < 4)
            buttons[key-1].push();
    }
}
