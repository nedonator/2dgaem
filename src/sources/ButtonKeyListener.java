package sources;

import App.App;
import Map.Map;
import MapEditor.MapEditorScene;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ButtonKeyListener implements KeyListener {
    private App app;
    private ButtonMenu scene;

    public ButtonKeyListener(App app, ButtonMenu scene) {
        this.app = app;
        this.scene = scene;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
            app.terminate();
        else if(Character.isDigit(e.getKeyChar())){
            scene.pressKey(Character.digit(e.getKeyChar(), 10));
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
