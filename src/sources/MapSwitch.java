package sources;

import App.App;
import Map.Map;
import Textures.MapTextures;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.List;

import static Config.Config.HEIGHT;
import static Config.Config.WIDTH;

public class MapSwitch implements Scene, ButtonMenu {
    private ButtonMouseListener mouseListener;
    private KeyListener keyListener;
    private Image image = MapTextures.get("map_switch");
    private List<String> maps = Arrays.asList(
            "hills"
    );
    private List<Integer> temperature = Arrays.asList(
            270
    );
    private int it = 0;
    private Image map = MapTextures.get("maps", maps.get(0));
    private Button[] buttons;
    private App app;
    private JFrame frame;

    public MapSwitch(App app) {
        this.app = app;
        this.frame = app.getFrame();
        buttons = new Button[3];
        buttons[0] = new Button(WIDTH/2-150, HEIGHT/2+200, 40, 40, MapTextures.get("left"), this::prev);
        buttons[1] = new Button(WIDTH/2+110, HEIGHT/2+200, 40, 40, MapTextures.get("right"), this::next);
        buttons[2] = new Button(WIDTH/2-100, HEIGHT/2+200, 200, 40, MapTextures.get("button"), this::select);
        mouseListener = new ButtonMouseListener(buttons, frame);
        keyListener = new ButtonKeyListener(app, this);
    }

    @Override
    public void update(double dt) {

    }

    @Override
    public void render(Graphics2D g) {
        g.translate(-WIDTH/2, -HEIGHT/2);
        g.drawImage(image, 0, 0, null);
        g.drawImage(map, WIDTH/2-300, 50, 600, 300, null);
        for(Button button : buttons)
            button.render(g);
        g.setColor(new Color(0, 0, 0));
        g.setFont(new Font("Dialog", Font.PLAIN, 36));
        g.drawString(maps.get(it), WIDTH/2-95, HEIGHT/2+235);
    }

    @Override
    public MouseListener getMouseListener() {
        return mouseListener;
    }

    @Override
    public KeyListener getKeyListener() {
        return keyListener;
    }

    @Override
    public void save() {

    }

    @Override
    public void pressKey(int key) {
        if(key > 0 && key < 4)
            buttons[key-1].push();
    }

    public void prev(){
        if(--it==-1)
            it = maps.size() - 1;
        map = MapTextures.get("maps", maps.get(it));
    }

    public void next(){
        if(++it==maps.size())
            it = 0;
        map = MapTextures.get("maps", maps.get(it));
    }

    public void select(){
        app.setScene(new BattleScene(app, app.getFrame(),
                Map.readFromFile("maps", maps.get(it)), 15, 0, temperature.get(it)));
    }
}
