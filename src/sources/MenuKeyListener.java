package sources;

import App.App;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.VK_ESCAPE;

public class MenuKeyListener implements KeyListener {
    private App app;

    public MenuKeyListener(App app){
        this.app = app;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == VK_ESCAPE){
            app.terminate();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
