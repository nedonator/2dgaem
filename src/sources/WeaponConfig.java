package sources;

import Weapons.Weapon;

import javax.swing.*;

public interface WeaponConfig {
    Weapon getWeapon(BattleScene scene, JFrame frame);
}
