package sources;

import App.App;
import Config.Config;
import Player.Player;
import Textures.MapTextures;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import Objects.Movable;
import Objects.Sprite;
import Objects.Enemy;
import Objects.Damageable;
import Objects.EnemyGenerator;
import Weapons.Bullet;
import Map.Layer;
import Map.Block;
import Map.Map;
import Player.PlayerConfig;


import static Config.Config.*;

public class BattleScene implements Scene {
    private final App app;
    private final Map map;
    private final Collection<Movable> objects;
    private final Collection<Sprite> sprites;
    private final Collection<Movable> markForDelete;
    private final Collection<Movable> markForCreate;
    private final Collection<Sprite> effects;
    private final Player player;
    private Layer[] background;
    private double temperature;
    private Sprite tempEffect = null;

    public BattleScene(App app, JFrame frame, Map map, int spawnX, int spawnY, double temperature) {
        this.app = app;
        this.map = map;
        objects = ConcurrentHashMap.newKeySet();
        sprites = ConcurrentHashMap.newKeySet();
        markForDelete = ConcurrentHashMap.newKeySet();
        markForCreate = ConcurrentHashMap.newKeySet();
        effects = ConcurrentHashMap.newKeySet();
        player = PlayerConfig.getPlayer(app, this, frame, spawnX, spawnY);
        objects.add(player);
        //for(int i = 0; i < 10; i++)
        //    objects.add(new Enemy(this, random.nextInt(), 0, MapTextures.get("Enemy"), 2, 3, 100, 10, 10));
        background = new Layer[1];
        //background[0] = new Layer(MapTextures.get("back"), 0.1, 0.1, player, 960);
        background[0] = new Layer(MapTextures.get("yoba"), 1, 1, player, 1400);
        this.temperature = temperature;
    }

    public int inspect(Movable[] list, int i, int it, boolean side) {
        if (list[i] instanceof Damageable) {
            it = i;
        } else if (list[i] instanceof Bullet && it != -1 && list[i].getX() < list[it].getX() ^ side) {
            Damageable d = (Damageable) list[it];
            Bullet b = (Bullet) list[i];
            d.tryHit(b);
        }
        return it;
    }

    @Override
    public void update(double dt) {
        if(random.nextDouble() * objects.size() < dt)
            markForCreate.add(EnemyGenerator.generate(this, random.nextInt(), random.nextDouble()*10, temperature));
        if(dt > 0.05)
        System.out.println(dt + " " + objects.size());
        for (Layer l : background)
            l.update(dt);
        /*if (random.nextDouble() < 5 * dt)
            sprites.add(new Sprite(this, 26, 17, 0, -1, MapTextures.get("sprite0"), 2));*/
        Movable[] list = new Movable[objects.size()];
        objects.toArray(list);
        Arrays.sort(list, Comparator.comparingDouble(x -> x.getX()));
        for (Movable object : objects) {
            object.update(dt);
            if(object instanceof Damageable){
                Damageable d = (Damageable) object;
                d.tempAffect(temperature, dt);
            }
            /*if(object.getY() > 300){
                object.setY(0);
                object.setVy(0);
            }*/
        }
        int it = -1;
        for (int i = 0; i != list.length; i++) {
            it = inspect(list, i, it, false);
        }
        it = -1;
        for(int i = list.length - 1; i != -1; i--){
            it = inspect(list, i, it, true);
        }
        for (Movable object : objects) {
            object.normalize();
        }
        for (Sprite s : sprites) {
            s.update(dt);
            if (!s.isAlive()) sprites.remove(s);
        }
        for(Sprite s : effects){
            s.update(dt);
            if (!s.isAlive()) effects.remove(s);
        }
        if(tempEffect != null){
            tempEffect.update(dt);
            if(!tempEffect.isAlive())tempEffect = null;
        }
        for (Movable o : markForDelete) {
            objects.remove(o);
            if(o instanceof Enemy){
                Enemy e = (Enemy)o;
                PlayerConfig.addExp(e.getExp());
            }
        }
        objects.addAll(markForCreate);
        markForDelete.clear();
        markForCreate.clear();
        if(player.getHp() <= 0)
            app.setScene(new DeathScene(app));
    }

    @Override
    public void render(Graphics2D g){
        g.setColor(Temperature.color(temperature));
        g.fillRect(-WIDTH/2, -HEIGHT/2, WIDTH, HEIGHT);
        for(Layer l : background)
            l.render(g);
        g.translate(-player.getX()* BLOCK_SIZE, -player.getY()* BLOCK_SIZE);
        map.render(g);
        for(Movable object : objects)
            object.render(g);
        player.render(g);
        for(Sprite s : sprites)
            s.render(g);
        g.setColor(new Color(186, 186, 186));
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1));
        g.translate(player.getX()* BLOCK_SIZE, player.getY()* BLOCK_SIZE);
        for(Sprite s : effects){
            s.render(g);
        }
        if(tempEffect != null){
            tempEffect.render(g);
        }
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1));
        g.fillRect(10 - WIDTH/2, -55, 10, 110);
        g.fillRect(10 - WIDTH/2, HEIGHT/2 - 30, 220, 20);
        int mana = (int) (player.getMana() * 100 / player.getMaxMana());
        g.setColor(new Color(0, 255, 255));
        g.fillRect(12 - WIDTH/2, 50 - mana, 6, mana);
        g.setColor(new Color(255, 0, 0));
        int hp = (int)(player.getHp() * 100 / player.getMaxHp());
        g.fillRect(20 - WIDTH/2, HEIGHT/2 - 26, hp*2, 12);
        g.setColor(new Color(0, 255, 255));
        g.setFont(new Font("Dialog", Font.PLAIN, 36));
        g.drawString("" + player.getAmmo(), WIDTH/2 - 100, HEIGHT/2 - 15);
        g.setColor(new Color(255, 255, 255));
        g.drawString("temp: " + (int)temperature, WIDTH/2 - 200, -HEIGHT/2 + 35);

    }

    public int getType(int i, int j){
        return map.getBlock(i, j) != null ? map.getBlock(i, j).getType() : 0;
    }

    public void setBlock(Block block, int i, int j){
        map.setBlock(block, i, j);
    }

    @Override
    public KeyListener getKeyListener(){
        return player.getKeyListener();
    }

    @Override
    public MouseListener getMouseListener(){
        return player.getMouseListener();
    }

    public Map getMap() {
        return map;
    }

    public Collection<Sprite> getSprites() {
        return sprites;
    }

    public Player getPlayer() {
        return player;
    }

    public void create(Movable o){
        markForCreate.add(o);
    }

    public void delete(Movable o){
        markForDelete.add(o);
    }

    @Override
    public void save(){
        PlayerConfig.save();
    }

    public Collection<Sprite> getEffects() {
        return effects;
    }

    public void setTempEffect(Sprite tempEffect) {
        this.tempEffect = tempEffect;
    }
}
