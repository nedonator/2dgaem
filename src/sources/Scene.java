package sources;

import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

public interface Scene {
    void update(double dt);
    void render(Graphics2D g);
    MouseListener getMouseListener();
    KeyListener getKeyListener();
    void save();
}
