package sources;

import java.awt.*;

public class Button {
    private int x;
    private int y;
    private int w;
    private int h;
    private Image image;
    private Runnable action;

    public Button(int x, int y, int w, int h, Image image, Runnable action) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.image = image;
        this.action = action;
    }

    public void render(Graphics2D g) {
        if(image != null)
        g.drawImage(image, x, y, null);
        else{
            g.setColor(new Color(0, 0, 255));
            g.fillRect(x, y, w, h);
        }
    }

    public void tryPush(Point p){
        if(p.x >= x && p.x < x + w && p.y >= y && p.y < y + h)
            push();
    }

    public void push(){
        action.run();
    }
}
