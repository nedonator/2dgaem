package sources;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;

public class WASDKeyListener implements KeyListener {
    private boolean isWPressed = false;
    private boolean isAPressed = false;
    private boolean isSPressed = false;
    private boolean isDPressed = false;

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()){
            case VK_W : isWPressed = true;
                break;
            case VK_A : isAPressed = true;
                break;
            case VK_S : isSPressed = true;
                break;
            case VK_D : isDPressed = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()){
            case VK_W : isWPressed = false;
                break;
            case VK_A : isAPressed = false;
                break;
            case VK_S : isSPressed = false;
                break;
            case VK_D : isDPressed = false;
        }
    }

    public boolean isAPressed() {
        return isAPressed;
    }

    public boolean isDpressed() {
        return isDPressed;
    }

    public boolean isSpressed() {
        return isSPressed;
    }

    public boolean isWPressed() {
        return isWPressed;
    }
}
