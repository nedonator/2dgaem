package sources;

import java.awt.*;

public class Temperature {
    private static final double h = 6.62606957E-34;
    private static final double c = 299792458;
    private static final double k = 1.3806488E-23;
    private static final double RW = 625E-9;
    private static final double GW = 525E-9;
    private static final double BW = 460E-9;
    private static final double coef = 1e+3;
    private static final double max = 255;
    private static final double factor = 5;

    private static double plank(double wl, double temperature){
        return ((2*h*c)/(Math.pow(wl,5)))*(1/(Math.exp((h*c)/(wl*k*temperature))-1));
    }

    public static Color color(double temperature){
        //double r = plank(RW, temperature) * coef;
        //double g = plank(GW, temperature) * coef;
        //double b = plank(BW, temperature) * coef;
        double r = Math.pow(temperature/1000, 2) * max + Math.pow(temperature/1500, 4) * max + Math.pow(temperature/2000, 7) * max * 0.7;
        double g = Math.pow(temperature/1500, 4) * max + Math.pow(temperature/2000, 7) * max * 0.7;
        double b = Math.pow(temperature/2000, 7) * max;
        double n = Math.pow(Math.pow(r,factor)+Math.pow(g,factor)+Math.pow(b,factor)+Math.pow(max,factor) +
                Math.pow(1500 * max/temperature, factor), 1/factor);
        r *= max / n;
        g *= max / n;
        b *= max / n;
        //System.out.println(temperature + " :" + r + " " + g + " " + b + " ");
        return new Color((int)r, (int)g, (int)b);
    }
}
