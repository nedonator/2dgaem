package sources;

import App.App;
import Config.Config;
import Textures.MapTextures;

import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

public class DeathScene implements Scene {
    private final Image image = MapTextures.get("death");
    private final KeyListener keyListener;

    public DeathScene(App app){
        keyListener = new MenuKeyListener(app);
    }

    @Override
    public void update(double dt) {
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(image, -Config.WIDTH/2, -Config.HEIGHT/2, null);
    }

    @Override
    public MouseListener getMouseListener() {
        return null;
    }

    @Override
    public KeyListener getKeyListener() {
        return keyListener;
    }

    @Override
    public void save() {
    }
}
