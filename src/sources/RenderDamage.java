package sources;

import static Config.Config.*;
import Config.Config;
import Objects.Sprite;
import Textures.MapTextures;

import java.io.File;

public class RenderDamage {
    public static void render(BattleScene scene, int damage){
        damage += random.nextInt() % (damage / 2);
        while(damage > 100){
            int i = 1 + Math.abs(random.nextInt()) % 2;
            scene.getEffects().add(new Sprite(scene, random.nextInt() % (WIDTH/2/BLOCK_SIZE), random.nextInt() % (HEIGHT/2/BLOCK_SIZE), 0, 0,
                    MapTextures.get("DamageEffects" + File.separator + "heavy" + i), 20));
            damage -= 300;
        }
        while(damage > 10){
            int i = 1 + Math.abs(random.nextInt()) % 4;
            scene.getEffects().add(new Sprite(scene, random.nextInt() % (WIDTH/2/BLOCK_SIZE), random.nextInt() % (HEIGHT/2/BLOCK_SIZE), 0, 0,
                    MapTextures.get("DamageEffects" + File.separator + "medium" + i), 10));
            damage -= 30;
        }
        while(damage > 1){
            int i = 1 + Math.abs(random.nextInt()) % 4;
            scene.getEffects().add(new Sprite(scene, random.nextInt() % (WIDTH/2/BLOCK_SIZE), random.nextInt() % (HEIGHT/2/BLOCK_SIZE), 0, 0,
                    MapTextures.get("DamageEffects" + File.separator + "low" + i), 5));
            damage -= 3;
        }
    }
}
