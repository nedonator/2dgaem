package App;

import sources.Scene;

import javax.swing.*;

public interface App {
    void terminate();
    void update(int dt);
    boolean isRunning();
    void render();
    void setScene(Scene scene);
    JFrame getFrame();
}
