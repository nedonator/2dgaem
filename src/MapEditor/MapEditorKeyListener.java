package MapEditor;

import App.App;
import Map.Map;
import sources.WASDKeyListener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static Config.Config.MAP_HEIGHT;
import static Config.Config.MAP_WIDTH;

public class MapEditorKeyListener extends WASDKeyListener implements KeyListener {
    private int id = 1;
    private final MapEditorScene mapEditor;
    private final App app;

    public MapEditorKeyListener(App app, MapEditorScene mapEditor){
        this.mapEditor = mapEditor;
        this.app = app;
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        if(e.getKeyCode() == KeyEvent.VK_Q)
            id = 0;
        else if(e.getKeyCode() == KeyEvent.VK_F1){
            int[][] arr = new int[MAP_WIDTH][MAP_HEIGHT];
            for (int i = 0; i < MAP_WIDTH; i++) {
                for (int j = 0; j < MAP_HEIGHT; j++)
                    arr[i][j] = 0;
            }
            mapEditor.map = new Map(arr, null);
        }
        else if(e.getKeyCode() == KeyEvent.VK_F5){
            mapEditor.map.writeToFile("maps");
        }
        else if(Character.isDigit(e.getKeyChar())){
            int key = e.getKeyChar() - '0';
            id = id * 10 + key;
            if(id >= 10000) id = 0;
        }
        else if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            app.terminate();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {super.keyReleased(e);}

    public int getId(){
        return id;
    }
}
