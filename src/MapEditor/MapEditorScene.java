package MapEditor;

import App.App;
import Map.Block;
import Map.Map;
import sources.Scene;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import static Config.Config.*;

public class MapEditorScene implements Scene {
    public Map map;
    private double x = 0;
    private double y = 0;

    private int prevI;
    private int prevJ;
    private boolean usePrev = false;

    private final JFrame frame;
    private final MapEditorMouseListener mouseListener;
    private final MapEditorKeyListener keyListener;

    public MapEditorScene(App app){
        this.frame = app.getFrame();
        keyListener = new MapEditorKeyListener(app, this);
        mouseListener = new MapEditorMouseListener();
        map = new Map(MAP_WIDTH, MAP_HEIGHT);
    }

    @Override
    public void update(double dt){
        if(mouseListener.leftButtonPressed || mouseListener.rightButtonPressed){
            int id = mouseListener.leftButtonPressed ? keyListener.getId() : 0;
            Point p = frame.getMousePosition();
            if(p != null) {
                int i = (int) Math.floor((p.getX() + x) / BLOCK_SIZE);
                int j = (int) Math.floor((p.getY() + y) / BLOCK_SIZE);
                if(usePrev){
                    while(i < prevI){
                        map.setBlock(new Block(id), prevI--, j);
                    }
                    while(i > prevI){
                        map.setBlock(new Block(id), prevI++, j);
                    }
                    while(j < prevJ){
                        map.setBlock(new Block(id), i, prevJ--);
                    }
                    while(j > prevJ){
                        map.setBlock(new Block(id), i, prevJ++);
                    }
                }
                System.out.println("placing block at " + i + " " + j);
                map.setBlock(new Block(id), i, j);
                usePrev = true;
                prevI = i;
                prevJ = j;
            }
        } else usePrev = false;

        double d = MAP_EDITOR_SPEED * dt;
        if(keyListener.isAPressed())
            x -= d;
        if(keyListener.isDpressed())
            x += d;
        if(keyListener.isWPressed())
            y -= d;
        if(keyListener.isSpressed())
            y += d;
    }

    @Override
    public void render(Graphics2D g){
        g.translate(-WIDTH/2. - x, -HEIGHT/2. - y);
        map.render(g);
        g.translate(x, y);
        g.setColor(new Color(0, 0, 255));
        g.setFont(new Font("Dialog", Font.PLAIN, 36));
        g.drawString("block: " + keyListener.getId(), 0, 30);
    }

    @Override
    public MouseListener getMouseListener() {
        return mouseListener;
    }

    @Override
    public KeyListener getKeyListener() {
        return keyListener;
    }

    @Override
    public void save() {

    }
}
