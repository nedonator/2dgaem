package MapEditor;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MapEditorMouseListener implements MouseListener {
    public boolean leftButtonPressed = false;
    public boolean rightButtonPressed = false;

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            leftButtonPressed = true;
            rightButtonPressed = false;
        }
        else if(e.getButton() == MouseEvent.BUTTON3){
            leftButtonPressed = false;
            rightButtonPressed = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            leftButtonPressed = false;
        }
        else if(e.getButton() == MouseEvent.BUTTON3){
            rightButtonPressed = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
