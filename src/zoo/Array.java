package zoo;

import java.util.Arrays;

public class Array {
    public static void main(String[] args) {
        int[] a = {1, 0, 2, 3, 0, 4, 5};
        int n = a.length;
        int j = 0;
        for(int i = 0; i < n; i++){
            if(a[i] != 0){
                a[j] = a[i];
                if(j++ != i)a[i] = 0;
            }
        }
        System.out.println(Arrays.toString(a));
    }
}
