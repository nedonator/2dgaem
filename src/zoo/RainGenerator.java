package zoo;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import static Config.Config.*;

public class RainGenerator {
    public static BufferedImage generate(int width, int height, Color color, double speed, int density){
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics g = image.getGraphics();
        g.setColor(color);
        for(int i = 0; i < density; i++){
            double x = random.nextDouble() * width;
            double y = random.nextDouble() * height;
            double h = (y + speed) % height;
            if(h > y){
                g.drawLine((int)x, (int)y, (int)x, (int)h);
            }
            else{
                g.drawLine((int)x, (int)y, (int)x, height);
                g.drawLine((int)x, 0, (int)x, (int)h);
            }
        }
        return image;
    }

    public static void main(String[] args) throws Exception {
        BufferedImage image = generate(MAP_WIDTH * BLOCK_SIZE, MAP_HEIGHT * BLOCK_SIZE, new Color(2, 176, 220), 10, 1000);
        ImageIO.write(image, "png", new File("src/Textures/rain.png"));
    }
}
