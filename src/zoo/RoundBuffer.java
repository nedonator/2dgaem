package zoo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.RandomAccess;

public class RoundBuffer<T> {
    private final int n;
    private int i = 0;
    private Object[] arr;

    public RoundBuffer(int capacity, T filler){
        n = capacity;
        arr = new Object[n];
        Arrays.fill(arr, filler);
    }

    public void put(T o){
        arr[i++] = o;
        if(i == n)i = 0;
    }

    @SuppressWarnings("unchecked")
    public T get(int index){
        return (T)arr[(i + index) % n];
    }
}
