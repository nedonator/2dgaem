package zoo;

import java.util.HashMap;
import java.util.Map;

public class MapConstuctor {
    @SuppressWarnings("unchecked")
    public static <K,V> Map<K,V> construct(Object[] objects){
        if(objects.length % 2 == 1)
            throw new RuntimeException("input size is wrong");
        Map<K,V> map = new HashMap<>();
        for(int i = 0; i < objects.length; i += 2){
            map.put((K)objects[i], (V)objects[i+1]);
        }
        return map;
    }
}
