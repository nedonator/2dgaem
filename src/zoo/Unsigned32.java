package zoo;

public class Unsigned32 {
    private long val;
    private static long Unsigned32_MAX = (1L<<32) - 1;

    public Unsigned32(long val) {
        if(val < 0 || val > Unsigned32_MAX)
            throw new RuntimeException("value " + val + " out of bounds");
        this.val = val;
    }

    public Unsigned32 add(Unsigned32 other){
        long newVal = val + other.val;
        if(newVal > Unsigned32_MAX) newVal -= Unsigned32_MAX + 1;
        return new Unsigned32(newVal);
    }

    public Unsigned32 sub(Unsigned32 other){
        long newVal = val - other.val;
        if(newVal < 0) newVal += Unsigned32_MAX + 1;
        return new Unsigned32(newVal);
    }

    public long value(){
        return val;
    }

    @Override
    public String toString() {
        return Long.toString(val);
    }
}
