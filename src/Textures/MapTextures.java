package Textures;


import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MapTextures {
    private static Map<AbstractMap.SimpleEntry<String, String>, Image> map = new ConcurrentHashMap<>();
    public static Image get(String id){
        return get("textures", id);
    }

    public static Image get(String folder, String id){
        AbstractMap.SimpleEntry<String, String> entry = new AbstractMap.SimpleEntry<>(folder, id);
        Image image = map.get(entry);
        if(image != null)return image;
        else {
            String path = folder + File.separator + id + ".png";
            try {
                image = ImageIO.read(new File(path));
                map.put(entry, image);
                return image;
            } catch (Exception e) {
                System.out.println("[WARNING] null at MapTextures.get\nfilepath is " + path + "\n" + e.toString());
                return null;
            }
        }
    }
}
