import App.App;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Main {
    public static void main(String[] args) throws Exception {
        App app = new Game();
        long desiredFPS = 60;
        long desiredDeltaLoop = (1000*1000*1000)/desiredFPS;
        long beginLoopTime;
        long endLoopTime;
        long currentUpdateTime = System.nanoTime();
        long lastUpdateTime;
        long deltaLoop;
        System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream("logs.txt")), true));
        System.setErr(new PrintStream(new BufferedOutputStream(new FileOutputStream("errors.txt")), true));
        while(app.isRunning()){
            beginLoopTime = System.nanoTime();

            app.render();

            lastUpdateTime = currentUpdateTime;
            currentUpdateTime = System.nanoTime();
            app.update((int) ((currentUpdateTime - lastUpdateTime)/(1000*1000)));

            endLoopTime = System.nanoTime();
            deltaLoop = endLoopTime - beginLoopTime;

            if(deltaLoop < desiredDeltaLoop){
                try{
                    Thread.sleep((desiredDeltaLoop - deltaLoop)/(1000*1000));
                }catch(InterruptedException ignore){}
            }
        }
    }
}
